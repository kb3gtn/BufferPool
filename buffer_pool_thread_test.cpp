#include <iostream>
#include <thread>
#include <mutex>
#include <condition_variable>
#include <cstdint>
#include <atomic>

// make buffer pool print out details about what it is doing.
#define BUFFER_POOL_DEBUG 1
#include "buffer_pool.hpp"

using namespace std;


std::atomic<bool> isActive;


class BufferMailBox {
    BufferGuard buffer; // buffer object in mailbox
    atomic<bool> FullFlag; // 0 -- empty, 1 -- has message
    mutex sm; // signaling mutex
  public:

    BufferMailBox() {
        FullFlag = 0;
    }

    // Not Copy constructable due to BufferGuard and BufferDesc unique_ptr
    BufferMailBox( const BufferMailBox &) = delete;

    bool isEmpty() {
        bool e = not (FullFlag.load(std::memory_order_relaxed));
        return e;
    }

    bool isFull() {
        bool f = FullFlag.load(std::memory_order_relaxed);
        return f;
    }

    void enqueue(BufferGuard &bg) {
        // spin-lock on isFull signal, until it goes to not full
        while ( FullFlag.load(std::memory_order_relaxed) == true ) {
            std::this_thread::yield(); // sleep this thread..
        }
        // is ready for new data, grab lock
        lock_guard<mutex> lk(sm); // get mutex lock
        buffer.takeOver(bg);
        FullFlag.store(true, std::memory_order_relaxed);
    }

    void dequeue(BufferGuard &bg) {
        // spin-lock on isFull signal, until it goes to full
        while ( FullFlag.load(std::memory_order_relaxed) == false ) {
            std::this_thread::yield(); // sleep this thread..
        }
        // is ready to output a buffer
        lock_guard<mutex> lk(sm);
        bg.takeOver(buffer);
        FullFlag.store(false, std::memory_order_relaxed);
    }
};


// worker in a thread that takes a buffer and
// fills it with a counter.
void Worker_DataSource(BufferPool &p, shared_ptr<BufferMailBox> outbox) {

    while( isActive == true ) {

        // checkout a buffer from buffer pool to use for pipeline.
        BufferGuard buf( &p, p.checkout() );

        // Fill buffer with a 8bit counter.
        int buffer_len = buf.bdesc->getCapacity(); // get buffer capacity (max storage)
        uint8_t *dataptr = buf.bdesc->getData(); // get point to memory
                                                // Note memory is 16 byte aligned.
                                                // So casting to larger datatypes will work.

        // fill memory with repeating 8 bit counter.
        for (int i=0; i < buffer_len; ++i ) {
            dataptr[i] = i % 255;
        }

        // update used data in buffer. (we used the whole buffer)
        buf.bdesc->update_length(buffer_len);

        // enqueue created buffer into outbox.
        // this block until outbox is empty..
        outbox->enqueue( buf );

    }

    cout << "Worker Data Source Thread has Ended." << endl;
}

// takes in buffers does stuff to it.
// This work takes each byte and subtracts 10
void Worker_Processor1(shared_ptr<BufferMailBox> inbox, shared_ptr<BufferMailBox> outbox) {
    BufferGuard buf;
    while ( isActive == true ) {
        // get a buffer from the inbox
        inbox->dequeue(buf);

        // get buffer configuration info.
        int buffer_len = buf.bdesc->getCapacity(); // get buffer capacity
        uint8_t *dataptr = buf.bdesc->getData(); // get point to memory

        // do Work
        for (int i=0; i < buffer_len; ++i ) {
            dataptr[i] = dataptr[i] - 10;
        }

        // enqueue result in outbox
        outbox->enqueue( buf );
    }
    cout << "Worker Processor1 Thread has Ended." << endl;
}

// takes in buffers does stuff to it.
// This worker takes each byte and shift right by 1 (div by 2)
void Worker_Processor2(shared_ptr<BufferMailBox> &inbox, shared_ptr<BufferMailBox> outbox) {
    BufferGuard buf;
    while ( isActive == true ) {
        // get a buffer from the inbox
        inbox->dequeue(buf);

        // get buffer configuration info.
        int buffer_len = buf.bdesc->getCapacity(); // get buffer capacity
        uint8_t *dataptr = buf.bdesc->getData(); // get point to memory

        // do Work
        for (int i=0; i < buffer_len; ++i ) {
            dataptr[i] = dataptr[i] >> 1;
        }

        // enqueue result in outbox
        // hands ownership of buffer to the mailbox.
        outbox->enqueue( buf );
    }
    cout << "Worker Processor2 Thread has Ended." << endl;
}

// takes in buffers last stage of pipeline.
void Worker_DataSink(shared_ptr<BufferMailBox> inbox) {
    while ( isActive == true ) {
        BufferGuard buf;
        // get a buffer from the inbox (take ownership of buffer in mailbox)
        inbox->dequeue(buf);

        // Non-trivial example would do something with the data here..
        // Blah Blah Blah Moop

        // at end of scope the bufferguard will return the buffer to the pool
    }
    cout << "Worker Data Sink Thread has Ended." << endl;
}


//  Layout
//  DataSource -> Mailbox1 -> Processor1 -> Mailbox2 -> Processor2 -> Mailbox3 -> DataSync
//

int main() {

    BufferPool bpool(8, 16384);

    // create mailboxes
    shared_ptr<BufferMailBox> MailBox1, MailBox2, MailBox3;

    // Signaling variable
    isActive = true;

    // create thread workers
    thread t1( Worker_DataSource, bpool, MailBox1 );
    //thread t2( Worker_Processor1, MailBox1, MailBox2 );
    //thread t3( Worker_Processor2, MailBox2, MailBox3 );
    //thread t4( Worker_DataSink, MailBox3 );

    // wait a bit.
    std::this_thread::sleep_for(std::chrono::seconds(2) );

    // signal stop of app
    isActive = false;

    return 0;
}
