#include <iostream>
#include <unistd.h>
// enable debug print statements for buffers
#define BUFFER_POOL_DEBUG 1
#include "buffer_pool.hpp"

using namespace std;

#define BufferMaxSize 65536

void printBufferSpec(unique_ptr<BufferDesc> &b) {
    cout << "buffer info:" << endl;
    cout << "  Buffer ID#          : " << b->getID() << endl;
    cout << "  Buffer Capacity     : " << b->getCapacity() << " Bytes" << endl;
    cout << "  Buffer Used         : " << b->getLength() << " Bytes" << endl;
    uint8_t *dp = b->getData();
    cout << "  Buffer DataPtr Addr : " << hex << static_cast<void*>(dp) << dec << endl;
    cout << endl;
}

int main(int argc, char *argv[] )
{
    cout << endl << "Application Started.." << endl;
    cout << "Buffer Pool Construction begin:" << endl;
    cout << "-----------------------------------------------------" << endl;
    BufferPool bpool(4, BufferMaxSize ); // 4 buffers of 64KB
    cout << "----- Construction End ------- " << endl;
    cout << endl;
    cout << "----- Test Begin ------------- " << endl;

    cout << "Checking out a guarded buffer object in a local scope" << endl;

    // start local scope area
    {
      // create a guarded buffer to hold buffer checked out.
      BufferGuard bg1( &bpool, bpool.checkout() );
      // print out info about this buffer
      printBufferSpec( bg1.bdesc );
      // Buffer Guard should checkin buffer when it leaves scope.
      cout << "Available Buffers in Pool: " << bpool.buffersAvail() << endl;
    }
    // end local scope area..

    cout << "Buffer Guard destructor should have return the buffer." << endl;
    if ( bpool.buffersAvail() != 4 ) {
        cout << "Error Buffer Guard did not return the buffer to the pool. (FAIL)" << endl;
    } else {
        cout << "Buffer Guard returned buffer to pool on descope. (PASS) " << endl;
    }

    // check out all 4 buffers
    cout << "Checkout all buffers to empty pool.." << endl;
    BufferGuard bg1( &bpool, bpool.checkout() );
    BufferGuard bg2( &bpool, bpool.checkout() );
    BufferGuard bg3( &bpool, bpool.checkout() );
    BufferGuard bg4( &bpool, bpool.checkout() );

    // check that pool is depleted
    if ( bpool.buffersAvail() != 0 ) {
        cout << "Buffer Pool deplete test.. (FAIL)\n" << endl;
    } else {
        cout << "Buffer Pool deplete test..  (PASS)" << endl;
    }

    cout << "Test that checkouts on depleted pool using assign return error condition: " << endl;
    BufferGuard bg5;
    int result=bg5.assign(&bpool, bpool.checkout() );
    if ( result == 0) {
        cout << "Did not get expect return result for checkout of empty buffer. (FAIL) " << endl;
    } else {
        cout << "BufferGuard::Assign return correct value for buffer depletion. (PASS) " << endl;
    }

    // see if constructor throws if depleted buffer pool detected.
    cout << "Test that checkouts on depleted pool throw an exception.. " << endl;
    result=0;
    try {
        BufferGuard bg4( &bpool, bpool.checkout() );
    } catch (std::runtime_error &e ) {
        cout << "Caught Runtime Error: \"" << e.what() << "\"  (PASS) " << endl;
        result = 1;
    }
    if ( result == 0 ) {
        cout << "Failed to catch expected runtime error for depleted pool.  (FAIL)" << endl;
    }


    cout << "------ Test End ---------" << endl << endl;
    cout << "Application shutdown initiated:" << endl;
    cout << "-------- Start of Destruction ---------------" << endl;
}
