# Buffer Pool

Simple pool of buffers that can be "checked out" and "checked in" inlike books from a library.
Buffer Pool uses C++14 libraries and is written as a header-only implementation.

Buffer_Pool is thread safe with the following notes:
  * buffer pool uses locks for checkout and checkin
  * buffers passed between threads is lock-less
  * buffers are only "owned" by 1 thing at a time.
  * buffers updating the length field in a buffer is not thread safe.

Use of BufferGuard is not required, but prevents
memory leaks from occuring if buffers are not checked in
at the end of a scoped context.  Buffer Guard will
automatically checkin a buffer that it protects when
it goes out of scope.

Peter Fetterer


# Test Program output

<pre>
Application Started..
Buffer Pool Construction begin:
-----------------------------------------------------
BufferPool: Creating buffer #0
DEBUG: Buffer Created..
BufferPool: Creating buffer #1
DEBUG: Buffer Created..
BufferPool: Creating buffer #2
DEBUG: Buffer Created..
BufferPool: Creating buffer #3
DEBUG: Buffer Created..
----- Construction End ------- 
----- Test Begin ------------- 
Checking out a guarded buffer object in a local scope
buffer info:
  Buffer ID#          : 3
  Buffer Capacity     : 65536 Bytes
  Buffer Used         : 0 Bytes
  Buffer DataPtr Addr : 0xc50d00

Available Buffers in Pool: 3
BufferGuard: buffer ID #3 Cleanup Activated.
bufferPool: Checking in buffer ID #3
Buffer Guard destructor should have return the buffer.
Buffer Guard returned buffer to pool on descope. (PASS) 
Checkout all buffers to empty pool..
Buffer Pool deplete test..  (PASS)
Test that checkouts on depleted pool using assign return error condition: 
BufferGuard: given nullptr buffer to protect! (Depleted Pool?)
BufferGuard::Assign return correct value for buffer depletion. (PASS) 
Test that checkouts on depleted pool throw an exception.. 
BufferGuard: given nullptr from buffer poll.. Throw Pool Empty Exception.
Caught Runtime Error: "Buffer Pool Depleted"  (PASS) 
------ Test End ---------
Application shutdown initiated:
-------- Start of Destruction ---------------
BufferGuard is not procedting a buffer, simple cleanup..
BufferGuard: buffer ID #0 Cleanup Activated.
bufferPool: Checking in buffer ID #0
BufferGuard: buffer ID #1 Cleanup Activated.
bufferPool: Checking in buffer ID #1
BufferGuard: buffer ID #2 Cleanup Activated.
bufferPool: Checking in buffer ID #2
BufferGuard: buffer ID #3 Cleanup Activated.
bufferPool: Checking in buffer ID #3
BufferPool: Distructor Called, waiting for all buffers to checkin..
BufferPool: All buffers have been returned, starting cleanup of pool.
BufferPool: Freeing memory for buffer ID #0
BufferPool: Freeing memory for buffer ID #1
BufferPool: Freeing memory for buffer ID #2
BufferPool: Freeing memory for buffer ID #3
</pre>

