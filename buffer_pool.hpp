///////////////////////////////////////////////////////////////////
// Buffer_Pool.hpp
//
// Simple pool of buffers that can be checked out
// and checked in like books from a library.
//
// Buffer_Pool is thread safe with the following notes:
//  * buffer pool uses locks for checkout and checkin
//  * buffers passed between threads is lock-less
//  * buffers are only "owned" by 1 thing at a time.
//  * buffers updating the length field in a buffer is not thread safe.
//
// Use of BufferGuard is not required, but prevents
// memory leaks from occuring if buffers are not checked in
// at the end of a scoped context.  Buffer Guard will
// automatically checkin a buffer that it protects when
// it goes out of scope.
//
// Peter Fetterer (KB3GTN)
// kb3gtn@gmail.com
//
// License: www.dbad-license.org
//
///////////////////////////////////////////////////////////////////

#include <iostream>
#include <cstdlib>
#include <cstdint>
#include <cassert>
#include <memory>
#include <vector>
#include <cstring>
#include <stdexcept>
#include <mutex>

#ifndef BUFFER_POOL_DEBUG
#define BUFFER_POOL_DEBUG 0
#endif

using namespace std;

// forward declaration
class BufferPool;
class BufferGuard;

class BufferDesc {

    friend class BufferPool;
    friend class BufferGuard;

    uint32_t capacity;
    uint32_t length;
    uint8_t *dataptr;
    uint32_t id;
  public:

    uint8_t *getData() {
        return dataptr;
    }

    BufferDesc() {
        if ( BUFFER_POOL_DEBUG )
            cout << "DEBUG: Buffer Created.." << endl;
    }

    uint32_t getID() {
        return id;
    }

    uint32_t getCapacity() {
        return capacity;
    }

    uint32_t getLength() {
        return length;
    }

    uint32_t update_length(int _length) {
        length = _length;
    }

    uint32_t write( void *src, size_t bytes ) {
        size_t size = bytes;
        if ( (capacity - length) < size ) {
            // truncate write
            size = (capacity - length);
        }
        memcpy( src, &dataptr[length], size );
        return size;
    }

    // reset's length and clears buffer contents for reuse..
    void reset() {
        memset( dataptr, 0, capacity );
        length = 0;
    }

};


class BufferPool {

    // Pool information  (read-only after initalization..)
    uint32_t pool_size;
    uint32_t buffer_max_size; // must be a multiple of 16 bytes.

    // vector that holds a vector of BufferDesc(s)
    vector<unique_ptr<BufferDesc>> buffer_pool;
    // access mutex for this vector
    std::mutex pool_mutex;

    void initialize_buffers() {
        std::lock_guard<std::mutex> lock(pool_mutex); // mutex lock on vector buffer_pool
        for (int i=0; i < pool_size; ++i ) {
            if (BUFFER_POOL_DEBUG)
                cout << "BufferPool: Creating buffer #" << i << endl;
            unique_ptr<BufferDesc> b = std::make_unique<BufferDesc>();
            // allocate memory for this buffer
            // allocate memory starting on 16 byte boundary.
            b->dataptr = (uint8_t*)aligned_alloc(16, buffer_max_size );
            if ( b->dataptr == nullptr ) {
                if ( BUFFER_POOL_DEBUG )
                    cout << "BufferPool: Failed to allocate memory for buffer " << i << std::endl;
                return;
            }
            b->capacity = buffer_max_size;
            b->length = 0;
            b->id = i;
            // push created handle to buffer_pool vector
            buffer_pool.push_back(std::move(b));

        }
    }

  public:
    BufferPool(uint32_t _pool_size, uint32_t _buffer_max_size ) {
        pool_size = _pool_size;
        buffer_max_size = _buffer_max_size;
        initialize_buffers();
        pool_size = buffer_pool.size();
    }

    ~BufferPool() {
        if (BUFFER_POOL_DEBUG)
            cout << "BufferPool: Distructor Called, waiting for all buffers to checkin.." << endl;
        // wait for all buffer to be returned before cleaning up..
        // not locking on check on size.  Will become the correct value at some point after everything 
        // else as checked back in.  Grabbing the lock now, will block any check-ins occuring.
        while ( buffer_pool.size() != pool_size ) { /* spin lock */ } 
        // check-ins complete, now lock vector..
        // Free all buffer memory allocated in constructor.
        std::lock_guard<std::mutex> lock(pool_mutex); // mutex lock on vector buffer_pool
        if (BUFFER_POOL_DEBUG)
           cout << "BufferPool: All buffers have been returned, starting cleanup of pool." << endl; 
        for (int i=0; i < pool_size; ++i) {
            // free memory allocated in constructor.
            if (BUFFER_POOL_DEBUG)
                cout << "BufferPool: Freeing memory for buffer ID #" << buffer_pool[i]->id << endl;
            free(buffer_pool[i]->dataptr);
        }
    }

    uint32_t buffersAvail() {
        std::lock_guard<std::mutex> lock(pool_mutex); // mutex lock on vector buffer_pool
        return buffer_pool.size();
    }

    // get a buffer from the buffer pool (checkout)
    unique_ptr<BufferDesc> checkout() {
        std::lock_guard<std::mutex> lock(pool_mutex); // mutex lock on vector buffer_pool
        // check buffer pool not empty
        if (buffer_pool.empty())
            return unique_ptr<BufferDesc>(nullptr);
        // pop a buffer handle from buffer_pool vector
        unique_ptr<BufferDesc> b = std::move(buffer_pool.back());
        // buffer pool.back() will not contain a unique_ptr of nullptr
        buffer_pool.pop_back(); // remove the moved pointer from buffer_pool.
        // return unique_ptr
        return b;
    }

    // checkin / release buffer back to buffer pool
    // note: the b passed in will become nullptr after call.
    void checkin( unique_ptr<BufferDesc> &b ) {
        std::lock_guard<std::mutex> lock(pool_mutex); // mutex lock on vector buffer_pool
        // check that unique_ptr points to something (nullptr check)
        if ( b == nullptr ) {
            if (BUFFER_POOL_DEBUG)
                cout << "BufferPool: reject release of nullptr buffer." << endl;
            return;
        }
        // push back buffer onto buffer pool vector.
        if (BUFFER_POOL_DEBUG)
            cout << "bufferPool: Checking in buffer ID #" << b->id << endl;
        buffer_pool.push_back(std::move(b));
    }

};

// This class wraps a bufferdesc to provide clean in out-of-scope destructions
// In this case, when a buffer falls out of scope, it should be returned to bufferpool.
class BufferGuard {
    BufferPool *bpool;

  public:
    // buffer descriptor this object guards.
    unique_ptr<BufferDesc> bdesc;

    BufferGuard() { // guard, no asigned protectee
        bpool = nullptr;
        bdesc = nullptr;
    }

    BufferGuard( BufferGuard &b ) {
        bpool = b.bpool;
        bdesc = std::move(b.unguard());
    }

    BufferGuard( BufferPool *p, unique_ptr<BufferDesc> b ) {
        // check that we got a valid buffer.
        // buffer pool will return nullptr if pool is depleted
        if ( b == nullptr ) {
            cout << "BufferGuard: given nullptr from buffer poll.. Throw Pool Empty Exception." << endl;
            throw std::runtime_error(std::string("Buffer Pool Depleted"));
        }
        // buffer handle becomes owner of buffer.
        bdesc = std::move(b);
        // hold reference to bufferpool
        bpool = p;
    }

    ~BufferGuard() {
        if ( bpool == nullptr ) {
            if (BUFFER_POOL_DEBUG)
                cout << "BufferGuard is not procedting a buffer, simple cleanup.." << endl;
        } else {
            if (BUFFER_POOL_DEBUG)
                cout << "BufferGuard: buffer ID #" << bdesc->id << " Cleanup Activated." << endl;
            bpool->checkin(bdesc);
        }
    }

    void takeOver( BufferGuard &b ) {
        this->bpool = b.bpool;
        this->bdesc = std::move(b.bdesc);
        b.bpool = nullptr;
        b.bdesc = nullptr;
     }

    // unguard pointer, return underlaying unique_ptr to buffer descriptor.
    // used for passing a Guarded buffer to new owner.
    unique_ptr<BufferDesc> unguard() {
        if ( bpool == nullptr ) {
            if (BUFFER_POOL_DEBUG)
                cout << "BufferGuard::unguard() is not guarding a buffer, simple cleanup.." << endl;
        } else {
            if (BUFFER_POOL_DEBUG)
                cout << "BufferGuard::unguard(): buffer ID #" << bdesc->id << " Cleanup Activated." << endl;
            bpool->checkin(bdesc);
        }
        if (BUFFER_POOL_DEBUG)
            cout << "BufferGuard reset, not guarding a buffer." << endl;

        unique_ptr<BufferDesc> raw = std::move(bdesc);
        bpool = nullptr;
        bdesc = nullptr;
    }

    int assign( BufferPool *p, unique_ptr<BufferDesc> b ) {
        // check that we are not guarding anything already..
        if ( bpool != nullptr ) {
            if (BUFFER_POOL_DEBUG) {
                cout << "BufferGuard will not accept new guard, already assigned a buffer." << endl;
            }
            return -1;
        }
        // check that we got a valid buffer.
        // buffer pool will return nullptr if pool is depleted
        if ( b == nullptr ) {
            cout << "BufferGuard: given nullptr buffer to protect! (Depleted Pool?)" << endl;
            //throw std::runtime_error(std::string("Buffer Pool Depleted"));
            return -1;
        }
        if ( BUFFER_POOL_DEBUG )
            cout << "BufferGuard: setting up buffer, taken ownership of shared_ptr";
        // buffer handle becomes owner of buffer.
        bdesc = std::move(b);
        // hold reference to bufferpool
        bpool = p;
        return 0;
    }
};

